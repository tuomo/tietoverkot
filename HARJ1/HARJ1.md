Tietoverkot Analysaattorityö
===

## Harjoitus 1 - Verkon liikenteen kaappaaminen
1.1

    a) harj1_wireshark.pcap
    b) Siirrettiin 10 pakettia.

1.2

    a) harj12_wireshark.pcap
    b) Ei tarvita enää kolmitiekättelyä. Lisäksi kun sivu ei ole muuttunut, vastaa palvelin yksinkertaisesti HTTP-protokollan mukaisesti, että resurssi ei muuttunut edellisestä latauskerrasta. Tämän jälkeen asiakas vain kuittaa palvelimen HTTP-vastauksen. Yhteys myös säilyi auki, koska sivusto ladattiin uudelleen 30 sekunnin kuluessa. Siksi uutta kättelyä ei tarvittu.
    c) 14 pakettia, mukaanlukien yksi duplikaattikuittaus.