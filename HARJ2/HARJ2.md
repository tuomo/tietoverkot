Tietoverkot Analysaattorityö
===

## Harjoitus 2 - HTTP protokollan toimintaa 
3.

    a) 4 GET pyyntöä, mukaanlukien favicon.ico -pyyntö, jota ei varsinaisesti sivun lähdekoodista löytynyt (selain tekee automaattisesti /favicon.ico -pyynnön tässä tapauksessa)
    b) users.jyu.fi (130.234.10.207) ja www.jyu.fi (130.234.6.46)
    c) Näyttäisi tulevan peräkkäin, ei rinnakkain. Myös selaimen kehittäjätyökalut näyttäisivät vahvistavan tämän.
    d) harj2_3d.pcap

4.

    a) Koodilla 401 (Authorization Required)
    b) Kentässä WWW-Authenticate määritellään, mitä autentikointimenetelmää (Basic) ja sen perusteella koodausmenetelmää base64 (tieto RFC7617 dokumentista).
    c) harj2_4c.pcap
    d) HTTP voisi käyttää myös esim. Digest mnetelmää, missä tunnukset lähetetään hashatyssä muodossa. Tai NTLM -metodia, joka tekee haasteen, johon odottaa vastausta.

5.

    Harj2_5.pcap

    Paketteja lähetettiin yhteensä 13. Paketinkaappauksesta näkee, että merkkejä ei lähetetty palvelimelle yksi kerrallaan. Kaappauksesta myös käy ilmi, että HTTP-pyyntö onnistui, vastauksena tuli koodi 200.
    HUOM: telnet yhteys oli vielä auki, kun paketinkaappaus Wiresharkissa lopetettiin.
    Palvelin: example.com
    Käytetty käyttöjärjestelmä: macOS Sierra 10.12.6