Tietoverkot Analysaattorityö
===

## Harjoitus 3 - HTTP protokolla syvällisemmin

6.

    harj3_6.pcap

7.

    harj3_7_vainHTTP.pcap
    Wireshark on käsittääkseni tässä kasannut lähettämäni paketit HTTP-pyynnöksi, joka siis näkyy kyseisessä dump tiedostossa.

8.

    a) harj3_8a.pcap - jännästi palvelin vastaa HTTP/1.1 formaatissa, vaikka pyysin HTTP/1.0 GET pyynnössä. Ehkä palvelin ei tue HTTP/1.0 ja päivittää yhteyden automaattisesti HTTP/1.1:ksi?
    b) harj3_8b.pcap
    c) Connection: keep-alive -headerin ansiosta yhteyttä ei suljeta sen jälkeen, kun palvelin vastaa. Palvelin ilmoittaa timeout -määreeksi 5 sekuntia, joten pyynnöt on tehtävä todella nopeasti, tai palvelin ehtii sulkea yhteyden pyyntöjen välillä. Koska yhteyttä ei tarvitse muodostaa uudelleen, tulee yhteyden neuvottelua vähemmän - tehokkaampaa.
    d) harj3_8d.pcap - Tein HTTP GET pyynnöt samassa viestissä palvelimelle. Eli siis uusi pyyntö on myös laitettu, ennenkuin edellisestä saatu vastaus. Palvelin vastasi ymmärtääkseni samassa paketissa kummankin GET -pyynnön vastaukset.

9.

    a) harj3_9a_jyu_ensimmaiset.pcap
    b) harj3_9b_korppi_kaikki.pcap. Palautin kaikki paketit, koska paketteja aika vähän.
    c) Koska jyu.fi -sertifikaatti oli hyväksytty, pääsi selain hakemaan tietoja palvelimelta (Application data -paketit). Korppi.it.jyu.fi -sertifikaattia taas ei oltu hyväksytty, joten selain jäi neuvottelemaan palvelimen kanssa.