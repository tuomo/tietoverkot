Tietoverkot Analysaattorityö
===

## Harjoitus 4 - SMTP protokolla 

10.

    a)
    ### Asiakas:
    ```
    Yhdistetään...
    Connected to localhost (127.0.0.1) port: 25000
    220 ITKP104 Postipalvelin Sun Mar 17 19:18:51 EET 2019 
    >>> HELO tuomo\r\n
    250 ITKP104 Postipalvelin HELO localhost[127.0.0.1], good to see you! 
    >>> MAIL FROM: tuomo@testi.fi\r\n
    250 2.1.0 Sender...  tuomo@testi.fi you are ok
    >>> RCPT TO: vast@testi.fi\r\n
    250 2.1.5 Recipient...  vast@testi.fi is ok!
    >>> DATA\r\n
    354 Enter mail, end with "." on a line by itself
    >>> From: lähettäjä\r\n
    >>> Subject: otsikko\r\n
    >>> sisältöä
    >>> \r\n.\r\n
    250 2.0.0 Message displayed on Server screen.
    Disconnected from server localhost (127.0.0.1) port: 25000
    ```

    ### SMTP-palvelin:
    ```
    [127.0.0.1:49478]: HELO tuomo
    [127.0.0.1:49478]: MAIL FROM: tuomo@testi.fi
    [127.0.0.1:49478]: RCPT TO: vast@testi.fi
    [127.0.0.1:49478]: DATA
    [127.0.0.1:49478]: [sähköposti]
    From: lähettäjä
    Subject: otsikko
    sisältöä
    .
    [/sähköposti]
    ```

    ### Palvelimen yhteyksien tulostus (tämä vielä myös mukaan)
    ```
    (19:18:44) SMTP Palvelin käynnissä portissa: 25000
    (19:18:51) Uusi asiakas: 127.0.0.1 portista: 49478
    (19:20:17) Lopetti asiakas: 127.0.0.1 portista: 49478
    (19:20:19) Palvelin suljettu
    ```
    b) Tiedostossa `harj4_smtp.pcap`. `harj4_smtp.pcapng` on sama mutta eri tiedostomuodossa.