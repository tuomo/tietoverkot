# 11.

## a)
Tiedostossa `ftp_selain.pcap` on ftp-signalointi selaimella. `.pcapng` tiedosto on sama mutta toisessa tiedostoformaatissa.

## b)
Tiedostossa `ftp_asiakas.pcap` on ftp-signalointi tcp-asiakasohjelmalla. `.pcapng` tiedosto on sama mutta toisessa tiedostoformaatissa.