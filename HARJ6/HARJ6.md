# 12.

## a)
[SYN] SEQ=1487662757
[SYN, ACK] Seq=3174702856 Ack=1487662758
[ACK] Seq=1487662758 Ack=3174702857

## b)
MSS-Arvo on MSS=1460. Tämä on siis ensimmäisestä paketista TCP-neuvottelussa.
Alustava ikkunan koko on: Win=65535. Tämäkin on ensimmäisestä paketista TCP-neuvottelussa.

## c)
POST-pyynnön/metodin, tämä näkyy siis heti TCP-neuvotteluiden jälkeisessä paketissa.

## d)
