# 13.
## a)
Ping käyttää ICMP-protokollaa.
ICMP-protokollan muikaisesti
pyyntöviesti: ECHO (ping) request
vastaus: ECHO (ping) reply

Pakettikaappaus on tiedostossa ping.pcapng ja ping.pcap.

## b)
Example.com vastaa vielä 1472 tavun kokoisiin paketteihin. Kokeilin tämän pingaamalla antamatta paketin fragmentoitua, kunnes raja tuli vastaan.

## c)
Kahteen pakettiin. Wireshark pilkkoi sekä lähetyksen että vastauksen siis kahteen eri pakettiin,
joista ajallisesti jälkimmäisessä koottiin paketti aina kasaan.
Kaappaus yhdestä pingistä ja sen vastauksesta tiedostossa ping_fragmented.pcapng.

# 14.

## a)
Mac-koneeni lähettää UDP-paketteja, ja vastaus saadaan aina ICMP-protokollan mukaisesti.

## b)
Tracerouten käyttämät portit UDP-paketeissa:
Lähdeportti: 38785
Vastaanottajaportti: 33435

## c)
Vastaus tulee siis aina ICMP-protokollan mukaisesti.
Paketti sisältää viestin paketin elossaoloajan erääntymisestä:
Time-to-live exceeded (Time to live exceeded in transit)

## d)
Kaappaus löytyy `traceroute.pcapng` tiedostosta.